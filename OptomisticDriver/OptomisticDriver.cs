﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace OptomisticDriver
{
    public class OptomisticInstrument
    {
        string comPort = string.Empty;
        SerialPort serialPort = null;
        private string serialBuffer = string.Empty;      // buffer of incoming serial data
        private string readBuffer = string.Empty;        // buffer read on one event

        bool disposed = true;

        //--------------------------------
        // properties
        //----------------------------------
        private int wavelengthValue = 1;
        private int intensityValue = 1;


        public OptomisticInstrument(string port)
        {
            comPort = port;
        }


        public void Open(out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            errorOccurred = false;
            errorCode = 0;
            errorMsg = string.Empty;

            if (serialPort != null)                        // if already opne
            {
                serialPort.Close();                         // close it
                serialPort.Dispose();                       // and get rid of it
                this.comPort = string.Empty;
            }


            if (CheckForSerialPort(comPort))
            {
                // port valid on this system, so try to open
                try
                {
                    serialPort = new SerialPort(comPort, 19200);
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(serialDataReceived);
                    serialPort.ReadTimeout = 100;
                    serialPort.Open();           // open the serial port
                    disposed = false;
                }
                catch (Exception ex)
                {
                    errorMsg = "Error opening " + comPort + " : " + ex.Message;
                    errorCode = -1111;
                    errorOccurred = true;
                }
            }
            else
            {
                errorMsg = "Error opening " + comPort + " : Does not exist";
                errorCode = -1111;
                errorOccurred = true;
            }
            return;
        }

        public void Close()
        {
            if (serialPort != null)                        // if already opne
            {
                serialPort.Close();                         // close it
                serialPort.Dispose();                       // and get rid of it
                this.comPort = string.Empty;
            }
        }

        public void GetValues(out int intensity, out int frequency)
        {
            int x = 0;
            while ((wavelengthValue < 1000) &( x++ < 5))
            {
                System.Threading.Thread.Sleep(100);
            }

            intensity = intensityValue;
            frequency = wavelengthValue;
        }
        //---------------------
        //  utilities
        //-------------------
        private bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }

        static readonly object serialRcvLock = new object();      // setup so can lock code below
        //-------------------------------------
        // Method: DutSerialDataReceived
        //  StatusUpdated handler for the dut serial port
        //
        void serialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string msg = string.Empty;
            char[] delimiterChars = { ' ', '=', '\r', '\n' };
            Match m;
            SerialPort sp = (SerialPort)sender;
            string smsg = string.Empty;
            lock (serialRcvLock)
            {
                if (sp.IsOpen)
                {
                    try
                    {
                        wavelengthValue = 0;
                        intensityValue = 0;
                        msg = sp.ReadLine();
                        m = Regex.Match(msg, @"w=(\d+)");
                        if (m.Success)
                            int.TryParse(m.Value.Substring(2), out wavelengthValue);
                        m = Regex.Match(msg, @"i=(\d+)");
                        if (m.Success)
                            int.TryParse(m.Value.Substring(2), out intensityValue);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
            }
        }
    }
}
